//
//  main.m
//  GDTMob
//
//  Created by Teccente on 05/24/2019.
//  Copyright (c) 2019 Teccente. All rights reserved.
//

@import UIKit;
#import "GDTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GDTAppDelegate class]));
    }
}
