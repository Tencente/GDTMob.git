//
//  GDTAppDelegate.h
//  GDTMob
//
//  Created by Teccente on 05/24/2019.
//  Copyright (c) 2019 Teccente. All rights reserved.
//

@import UIKit;

@interface GDTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
