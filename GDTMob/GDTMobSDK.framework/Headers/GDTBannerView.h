//
//  GDTBannerView.h
//  GDTClient
//
//  Created by GaoChao on 13-12-30.
//  Copyright (c) 2018年 Tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GDTAdModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface GDTBannerView : UIView
@property (nonatomic, strong) GDTAdModel *adModel;
@property (nonatomic, strong) UIViewController *rootVc;
@end

NS_ASSUME_NONNULL_END
