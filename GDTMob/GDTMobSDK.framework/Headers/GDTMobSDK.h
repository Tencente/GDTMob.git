//
//  GDTMobSDK.h
//  GDTClient
//
//  Created by GaoChao on 13-12-30.
//  Copyright (c) 2018年 Tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GDTAdModel.h"
#import "GDTBannerView.h"
//广告控制类
NS_ASSUME_NONNULL_BEGIN

@interface GDTMobSDK : NSObject
//广告加载延时，默认是5秒
@property (nonatomic, assign) NSInteger delay;
+ (instancetype)shareInstance;
//默认展示windown 默认蒙层（最好跟启动页一样）完成回调 是否展示成功
- (void)loadAdAndShowInWindow:(UIWindow *)window adtype:(ADType)type withSplashView:(UIView *)view completionHandler:(void (^)(BOOL isOK,GDTAdModel *model,GDTBannerView *adView))completionHandler;

@end

NS_ASSUME_NONNULL_END
