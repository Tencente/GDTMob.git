# GDTMob

[![CI Status](https://img.shields.io/travis/Teccente/GDTMob.svg?style=flat)](https://travis-ci.org/Teccente/GDTMob)
[![Version](https://img.shields.io/cocoapods/v/GDTMob.svg?style=flat)](https://cocoapods.org/pods/GDTMob)
[![License](https://img.shields.io/cocoapods/l/GDTMob.svg?style=flat)](https://cocoapods.org/pods/GDTMob)
[![Platform](https://img.shields.io/cocoapods/p/GDTMob.svg?style=flat)](https://cocoapods.org/pods/GDTMob)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GDTMob is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GDTMob'
```

## Author

Teccente, zcodemobile@163.com

## License

GDTMob is available under the MIT license. See the LICENSE file for more info.
